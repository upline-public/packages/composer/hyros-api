<?php

namespace Uplinestudio\HyrosApi;

class CreateOrderResponse
{
    private string $requestId;
    private string $result;
    public function __construct($data)
    {
        $this->result = $data['result'];
        $this->requestId = $data['request_id'];
    }

    /**
     * @return string
     */
    public function getRequestId(): string
    {
        return $this->requestId;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }
}
