<?php

namespace Uplinestudio\HyrosApi;

class ErrorResponse
{
    private string $result;
    private array $message;
    public function __construct($data)
    {
        $this->result = $data['result'];
        $this->message = $data['message'];
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @return array
     */
    public function getMessage(): array
    {
        return $this->message;
    }
}
