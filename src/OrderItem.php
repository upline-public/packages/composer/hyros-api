<?php

namespace Uplinestudio\HyrosApi;

class OrderItem
{
    private string $name;
    private float $price;
    private ?string $externalId = null;
    private int $quantity = 0;
    private int $costOfGoods = 0;
    private int $taxes = 0;
    private ?int $itemDiscount = null;
    private ?array $packages = null;
    private bool $isRebill = false;

    /**
     * @param string $name
     * @param float $price
     */
    public function __construct(string $name, float $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     * @return OrderItem
     */
    public function setExternalId(?string $externalId): OrderItem
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return OrderItem
     */
    public function setQuantity(int $quantity): OrderItem
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return int
     */
    public function getCostOfGoods(): int
    {
        return $this->costOfGoods;
    }

    /**
     * @param int $costOfGoods
     * @return OrderItem
     */
    public function setCostOfGoods(int $costOfGoods): OrderItem
    {
        $this->costOfGoods = $costOfGoods;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaxes(): int
    {
        return $this->taxes;
    }

    /**
     * @param int $taxes
     * @return OrderItem
     */
    public function setTaxes(int $taxes): OrderItem
    {
        $this->taxes = $taxes;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getItemDiscount(): ?int
    {
        return $this->itemDiscount;
    }

    /**
     * @param int|null $itemDiscount
     * @return OrderItem
     */
    public function setItemDiscount(?int $itemDiscount): OrderItem
    {
        $this->itemDiscount = $itemDiscount;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getPackages(): ?array
    {
        return $this->packages;
    }

    /**
     * @param array|null $packages
     * @return OrderItem
     */
    public function setPackages(?array $packages): OrderItem
    {
        $this->packages = $packages;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRebill(): bool
    {
        return $this->isRebill;
    }

    /**
     * @param bool $isRebill
     * @return OrderItem
     */
    public function setIsRebill(bool $isRebill): OrderItem
    {
        $this->isRebill = $isRebill;
        return $this;
    }
}
