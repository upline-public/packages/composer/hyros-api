<?php

namespace Uplinestudio\HyrosApi;

class ApiCredentials
{
    private string $api_key;

    /**
     * @param string $api_key
     */
    public function __construct(string $api_key)
    {
        $this->api_key = $api_key;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->api_key;
    }
}
