<?php

namespace Uplinestudio\HyrosApi;

use GuzzleHttp\ClientInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;

class ApiClient
{
    const URL = 'https://api.hyros.com/v1/api/v1.0/orders';
    private ApiCredentials $credentials;
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;
    private ClientInterface $client;

    /**
     * @param ApiCredentials $credentials
     * @param RequestFactoryInterface $requestFactory
     * @param StreamFactoryInterface $streamFactory
     * @param ClientInterface $client
     */
    public function __construct(
        ApiCredentials $credentials,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface $streamFactory,
        ClientInterface $client)
    {
        $this->credentials = $credentials;
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
        $this->client = $client;
    }


    /**
     * @param Order $order
     * @return CreateOrderResponse|ErrorResponse
     * @throws NotFoundException|ClientExceptionInterface|WrongResponseException
     */
    public function createOrder(Order $order)
    {
        $data = $this->conversionOrderToArray($order);
        $response = $this->jsonRequest($data);

        return $this->isResponseSuccess($response) ? new CreateOrderResponse($response) : new ErrorResponse($response);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws WrongResponseException
     */
    public function jsonRequest($data, $method = 'POST')
    {
        $request = $this->requestFactory
            ->createRequest($method, self::URL)
            ->withAddedHeader('Content-type', 'application/json')
            ->withAddedHeader('API-Key', $this->credentials->getApiKey())
            ->withBody($this->streamFactory->createStream(json_encode($data)))
        ;

        $response = $this->client->sendRequest($request);

        if ($response->getStatusCode() === 404) {
            throw new NotFoundException('Page not found', $response->getStatusCode());
        } else if (!in_array($response->getStatusCode(), [200, 201])) {
            throw new WrongResponseException('Wrong Response (status code ' . $response->getStatusCode() . '): ' . $response->getBody()->getContents(), $response->getStatusCode());
        }

        return $this->decodeResponse($response);
    }

    public function decodeResponse($response) {
        return json_decode($response->getBody()->getContents(), true);
    }

    private function isResponseSuccess($response): bool
    {
        return $response['result'] === 'OK';
    }

    private function conversionOrderToArray(Order $order): array
    {
        $orderItems = $order->getItems();
        return [
            'email' => $order->getEmail(),
            'firstName' => $order->getFirstName(),
            'lastName' => $order->getLastName(),
            'leadIps' => $order->getLeadIps(),
            'phoneNumbers' => $order->getPhoneNumbers(),
            'orderId' => $order->getOrderId(),
            'date' => $order->getDate(),
            'priceFormat' => $order->getPriceFormat(),
            'currency' => $order->getCurrency(),
            'taxes' => $order->getTaxes(),
            'shippingCost' => $order->getShippingCost(),
            'orderDiscount' => $order->getOrderDiscount(),
            'items' => array_map(function (OrderItem $item) {
                return [
                    'name' => $item->getName(),
                    'price' => $item->getPrice(),
                    'externalId' => $item->getExternalId(),
                    'quantity' => $item->getQuantity(),
                    'costOfGoods' => $item->getCostOfGoods(),
                    'taxes' => $item->getTaxes(),
                    'itemDiscount' => $item->getItemDiscount(),
                    'packages' => $item->getPackages(),
                    'isRebill' => $item->isRebill()
                ];
            }, $orderItems)
        ];
    }
}
