<?php

namespace Uplinestudio\HyrosApi;

class Order
{
    /**
     * @var string
     */
    private string $email;
    /**
     * @var array<OrderItem>
     */
    private array $items;

    private ?string $firstName = null;
    private ?string $lastName = null;
    private ?array $leadIps = null;
    private ?array $phoneNumbers = null;
    private ?string $orderId = null;
    private ?string $date = null;
    private int $shippingCost = 0;
    private int $taxes = 0;
    private int $orderDiscount = 0;
    private ?string $priceFormat = null;
    private ?string $currency = null;

    public function __construct(string $email, array $items)
    {

        $this->email = $email;
        $this->items = $items;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return Order
     */
    public function setFirstName(?string $firstName): Order
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return Order
     */
    public function setLastName(?string $lastName): Order
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getLeadIps(): ?array
    {
        return $this->leadIps;
    }

    /**
     * @param array|null $leadIps
     * @return Order
     */
    public function setLeadIps(?array $leadIps): Order
    {
        $this->leadIps = $leadIps;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getPhoneNumbers(): ?array
    {
        return $this->phoneNumbers;
    }

    /**
     * @param array|null $phoneNumbers
     * @return Order
     */
    public function setPhoneNumbers(?array $phoneNumbers): Order
    {
        $this->phoneNumbers = $phoneNumbers;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderId(): ?string
    {
        return $this->orderId;
    }

    /**
     * @param string|null $orderId
     * @return Order
     */
    public function setOrderId(?string $orderId): Order
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     * @return Order
     */
    public function setDate(?string $date): Order
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getShippingCost(): int
    {
        return $this->shippingCost;
    }

    /**
     * @param int $shippingCost
     * @return Order
     */
    public function setShippingCost(int $shippingCost): Order
    {
        $this->shippingCost = $shippingCost;
        return $this;
    }

    /**
     * @return int
     */
    public function getTaxes(): int
    {
        return $this->taxes;
    }

    /**
     * @param int $taxes
     * @return Order
     */
    public function setTaxes(int $taxes): Order
    {
        $this->taxes = $taxes;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderDiscount(): int
    {
        return $this->orderDiscount;
    }

    /**
     * @param int $orderDiscount
     * @return Order
     */
    public function setOrderDiscount(int $orderDiscount): Order
    {
        $this->orderDiscount = $orderDiscount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPriceFormat(): ?string
    {
        return $this->priceFormat;
    }

    /**
     * @param string|null $priceFormat
     * @return Order
     */
    public function setPriceFormat(?string $priceFormat): Order
    {
        $this->priceFormat = $priceFormat;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     * @return Order
     */
    public function setCurrency(?string $currency): Order
    {
        $this->currency = $currency;
        return $this;
    }
}
