<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\HttpFactory;
use Uplinestudio\HyrosApi\ApiClient;
use Uplinestudio\HyrosApi\ApiCredentials;
use Uplinestudio\HyrosApi\NotFoundException;
use Uplinestudio\HyrosApi\Order;
use Uplinestudio\HyrosApi\OrderItem;
use Uplinestudio\HyrosApi\WrongResponseException;

require_once dirname(__DIR__) . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$orderItem = new OrderItem('T-Shirt - Blue', 15.50);
$orderItems = [
    $orderItem
        ->setCostOfGoods(2.25)
        ->setExternalId('18294892740')
        ->setQuantity(2)
        ->setTaxes(2.5)
        ->setPackages(["Package 1", "Package 2"])
        ->setIsRebill(true)
];
$order = new Order('test@test.com', $orderItems);

$order
    ->setFirstName('Test')
    ->setLastName('Test');

$client = new ApiClient(
    new ApiCredentials($_ENV['API_KEY']),
    new HttpFactory(),
    new HttpFactory(),
    new Client()
);

try {
    $res = $client->createOrder($order);
    var_dump($res->getResult()); // expected 'OK'
} catch (WrongResponseException|NotFoundException $e) {
    var_dump($e->getMessage());
}
